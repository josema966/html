Visualino
=========

Entorno de programación visual por bloques para Arduino

Requirimientos
------------

Se necesita tener instalado Arduino IDE en una versión mayor a la 1.6

Descaga de Visualino
--------------------

Lo descargamos de la página original del proyecto
[Visualino downloads](http://www.visualino.net/downloads/)

Instalación
-----------

Elegimos el sistema operativo que utilicemos.
**Windows** Debemos descomprimir el archivo
**Linux** Se distribuye como un paquete de Debian por lo que cuando lo bajemos nos pedirá permiso para instalarlo.
**iOS** Debe pasar algo parecido a la opción de Linux


Añadir los bloques
------------------

Para añadir los bloques nuevos, para programacion del Escornabot, Evolution, la placa Multi-function Shield y más que se añadirán hacemos lo siguiente:

Descarga del repo
-----------------

Al descargar el repo, tenemos todo lo necesario para utilizar los bloque. Hay dos maneras de bajar Visualino:

* Descarga con git
-------------------

Es la recomendada ya que con una simple orden puedes actualizar el programa, corregir posibles fallos y tener los últimos bloques.

Para **windows** deberás descargarte previamente el programa *git* o alguna aplicación de escritorio que maneje el sistema de repositorios *git*. El recomendado por los desarolladores de git es http://git-scm.com/download/win
 
Desde un terminal o desde la ventana de comandos de windows ponemos (podemos copiar y pegar) nos vamos a la carpeta /visualino-0.7.1 y desde allí ponemos:

``` 
git clone https://gitlab.com/josema966/html.git
```
 
Para **Linux o iOS** no es necesario descargarse *git* ya que viene en todas las distribuciones. Tendremos que trabajar un poquito distinto. Abrimos un terminal y desde cualquier directorio ponemos:

``` 
git clone https://gitlab.com/josema966/html.git
cp ./html *ruta_a_la_carpeta_del_mismo_nombre_donde_se_instaló_visualino*
sudo cp -rf ./html /usr/share/visualino/
```
Si nos dice que ya hay una carpeta con ese nombre, aceptamos para que sobreescriba los archivos. Copiando la carpeta a estos dos sitios nos aseguramos que Visualino coge los bloques que queremos

Cada vez que queramos obtener una actualización deberemos de poner:

```
git pull
```

* Descarga con zip
------------------
Es más sencilla porque estamos acostumbrados a hacerlo. Tiene el inconveniente de que las actualizaciones no son tan sencillas pues somos nosotros quienes debemos llevar el control de lo que se actualiza o no.

Instalación
-----------

**Para linux**
Lo primero tienes que copiar la carpeta html que está dentro de la que acabas de descargar a /usr/share/visualino y sobreescribir los archivos que hay en ella.
De igual manera sobreescribe la carpeta html que hay en el directorio de instalación de Visualino.

**Para windows**

Descomprimid el archivo ZIP y copiad la carpeta html en la carpeta de instalación. Algo parecido a : /visualino-0.7.1 y autorizad a que se sobreescriban los archivos.

Ejecución
---------

En Linux tendreis un acceso directo en vuestros menús
En Windows ejecutad el .exe que ha en la carpeta /visualino-0.7.1


Configuración
-------------

Es muy importante decirle a Visualino donde tenemos instalado el Arduino IDE para que lo utilice. Los/las Alumnos/as no están obligados a manejar el Arduino IDE. Visualino lo utiliza de forma transparente al usuario.

Dentro de Visualino picamos en la rueda dentada. Se nos abre una caja de diálogo donde pondremos la dirección de la carpeta donde tengamos el Arduino IDE. Asimismo pondremos tambien el idioma deseado.


Actualizaciones
---------------

Si quieres puedes bajarte el proyecto original con todos los fuentes y todo desde [GitHub de Victor Ruiz](https://github.com/vrruiz/visualino)


Support
-------

Go to the [http://www.visualino.net/forum/](forum).


Developer
---------

Víctor R. Ruiz <rvr@linotipo.es>

Modified by
-----------

@itubalibre

Credits
-------

* [https://developers.google.com/blockly/](Google Blockly).
* [https://github.com/bq/roboblocks](bq's roboblocks).


License
-------

Check LICENSE file.

